const BASE_URL = '/v1'

export default  {
    DO_SEARCH_CONTENT: `${BASE_URL}/board/all/{pageNum}`, // get: 보드 모두 가져오기
    DO_DETAIL_CONTENT: `${BASE_URL}/board/detail/board-id/{boardId}`, // get: 보드 상세보기
    DO_DELETE_CONTENT: `${BASE_URL}/delete/board-fk/all/{boardId}`, // del: 보드 삭제하기
}
