export default {
    DO_SEARCH_CONTENT: 'topic/allPaging',
    DO_DETAIL_CONTENT: 'topic/detail-member',
    DO_DELETE_CONTENT: 'topic/delete',
}
