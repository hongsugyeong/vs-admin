import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_SEARCH_CONTENT]: (store, payload) => {
        return axios.get(apiUrls.DO_SEARCH_CONTENT.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_DETAIL_CONTENT]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL_CONTENT.replace('{boardId}', payload.id))
    },
    [Constants.DO_DELETE_CONTENT]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE_CONTENT.replace('{boardId}', payload.id))
    },
}
