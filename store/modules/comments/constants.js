export default {
    DO_SEARCH_COMMENTS: 'comments/allPaging',
    DO_DELETE_COMMENTS: 'comments/delete',
    DO_DETAIL_COMMENTS: 'comments/detail-member',
}
