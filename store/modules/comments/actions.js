import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_SEARCH_COMMENTS]: (store, payload) => {
        return axios.get(apiUrls.DO_SEARCH_COMMENTS.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_DELETE_COMMENTS]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE_COMMENTS.replace('{commentId}', payload.id))
    },
    [Constants.DO_DETAIL_COMMENTS]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL_COMMENTS.replace('{commentId}', payload.id))
    }
}
