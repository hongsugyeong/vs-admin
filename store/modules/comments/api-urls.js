const BASE_URL = '/v1/comment'
const BASE_URL_DELETE = '/v1'

export default  {
    DO_SEARCH_COMMENTS: `${BASE_URL}/all/{pageNum}`, // get: 댓글 모두 가져오기
    DO_DELETE_COMMENTS: `${BASE_URL_DELETE}/delete/comment-fk/all/{commentId}`, // del: 댓글 삭제하기
    DO_DETAIL_COMMENTS: `${BASE_URL}/detail/comment-id/{commentId}`, // get: 해당 댓글단 게시글 상세보기
}
