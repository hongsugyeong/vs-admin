import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'
import {store} from "core-js/internals/reflect-metadata";

export default {
    [Constants.SEARCH_ALL]: (store, payload) => {
        return axios.get(apiUrls.SEARCH_ALL.replace('{pageNum}', payload.pageNum))
    },
    [Constants.SEARCH_ID]: (store, payload) => {
        return axios.get(apiUrls.SEARCH_ID.replace('{id}', payload.id))
    },
    [Constants.DO_MEMBER_WRITE_ALL]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER_WRITE_ALL.replace('{memberId}', payload.id))
    },
    [Constants.DO_MEMBER_COMMENTS_ALL]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER_COMMENTS_ALL.replace('{memberId}', payload.id))
    },
    [Constants.DO_MEMBER_VOTE_ALL]: (store, payload) => {
        return axios.get(apiUrls.DO_MEMBER_VOTE_ALL.replace('{memberId}', payload.id))
    },
    [Constants.DO_CHANGE_GRADE]: (store, payload) => {
        return axios.put(apiUrls.DO_CHANGE_GRADE.replace('{id}', payload.id), payload.data)
    },
}
