const BASE_URL = '/v1/member'
const BASE_URL_MEMBER_CLOSER = '/v1'


export default  {
    SEARCH_ALL: `${BASE_URL}/all/{pageNum}`, // get: 회원 리스트로 가져오기
    SEARCH_ID: `${BASE_URL}/detail/id/{id}`, // get: 회원 상세 정보 가져오기
    DO_MEMBER_WRITE_ALL: `${BASE_URL_MEMBER_CLOSER}/board/member-id/{memberId}/all`, // get: 회원이 작성한 게시글 가져오기
    DO_MEMBER_COMMENTS_ALL: `${BASE_URL_MEMBER_CLOSER}/comment/member-id/{memberId}`, // get: 회원이 작성한 댓글 가져오기
    DO_MEMBER_VOTE_ALL: `${BASE_URL_MEMBER_CLOSER}/vote-board/member-id/{memberId}/all`, // get: 회원이 투표한 내역 가져오기
    DO_CHANGE_GRADE: `${BASE_URL}/change-grade/{id}`, // put: 회원 등급 수정하기
}
