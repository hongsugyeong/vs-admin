export default {
    SEARCH_ALL: 'member/allPaging',
    SEARCH_ID: 'member/detail-member-topic',
    DO_MEMBER_WRITE_ALL: 'member/write/all',
    DO_MEMBER_COMMENTS_ALL: 'member/comments/all',
    DO_MEMBER_VOTE_ALL: 'member/vote/all',
    DO_CHANGE_GRADE: 'member/change/grade',
}
