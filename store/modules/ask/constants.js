export default {
    DO_ASK_LIST: 'ask/doAskList',
    DO_ASK_LIST_PAGING: 'ask/doAskListPaging',
    DO_ASK_DETAIL: 'ask/doAskDetail',
    DO_ASK_CREATE: 'ask/doAskCreate',
    DO_ASK_DELETE: 'ask/doAskDelete',
    DO_ASK_LIST_MANAGE: 'ask/doAskListManage',
    DO_ASK_LIST_MANAGE_DETAIL: 'ask/doAskListManageDetail',
    DO_ASK_LIST_MANAGE_DELETE: 'ask/doAskListManageDelete',
}
