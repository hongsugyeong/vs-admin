const BASE_URL = '/v1/member-ask'
const BASE_URL1 = '/v1/ask-management'

export default {
    DO_ASK_LIST: `${BASE_URL}/all`, //get
    DO_ASK_LIST_PAGING: `${BASE_URL}/all/{pageNum}`, //get
    DO_ASK_DETAIL: `${BASE_URL}/detail/memberAsk-id/{memberAskId}`, //get
    DO_ASK_CREATE: `${BASE_URL1}/join/member-ask-id/{memberAskId}`, //post
    DO_ASK_DELETE: `${BASE_URL}/delete/{id}`, //del
    DO_ASK_LIST_MANAGE: `${BASE_URL1}/{pageNum}/all`, // get: 관리자가 답변한 문의글 다 가져오기
    DO_ASK_LIST_MANAGE_DETAIL: `${BASE_URL1}/detail/ask-management-id/{askManagementId}`, // get: 관리자 답변 내역 상세보기
    DO_ASK_LIST_MANAGE_DELETE: `${BASE_URL1}/delete/ask-management-id/{askManagementId}`, // del: 관리자 답변 내역 삭제하기
}
