import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_ASK_LIST]: (store) => {
        return axios.get(apiUrls.DO_ASK_LIST)
    },
    [Constants.DO_ASK_LIST_PAGING]: (store, payload) => {
        return axios.get(apiUrls.DO_ASK_LIST_PAGING.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_ASK_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_ASK_DETAIL.replace('{memberAskId}', payload.id))
    },
    [Constants.DO_ASK_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_ASK_CREATE.replace('{memberAskId}', payload.id), payload.data)
    },
    [Constants.DO_ASK_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_ASK_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_ASK_LIST_MANAGE]: (store, payload) => {
        return axios.get(apiUrls.DO_ASK_LIST_MANAGE.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_ASK_LIST_MANAGE_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_ASK_LIST_MANAGE_DETAIL.replace('{askManagementId}', payload.id))
    },
    [Constants.DO_ASK_LIST_MANAGE_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_ASK_LIST_MANAGE_DELETE.replace('{askManagementId}', payload.id))
    },
}
