export default {
    DO_FORBID_LIST: 'forbid/doForbidList',
    DO_FORBID_PAGE_LIST: 'forbid/doForbidPageList',
    DO_FORBID_DELETE: 'forbid/doForbidDelete',
    DO_FORBID_CREATE: 'forbid/doForbidCreate'
}
