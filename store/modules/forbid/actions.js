import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_FORBID_LIST]: (store) => {
        return axios.get(apiUrls.DO_FORBID_LIST)
    },
    [Constants.DO_FORBID_PAGE_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_FORBID_PAGE_LIST.replace('{pageNum}', payload.pageNum))
    },
    [Constants.DO_FORBID_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_FORBID_DELETE.replace('{forbidId}', payload.id))
    },
    [Constants.DO_FORBID_CREATE]: (store, payload) => {
        return axios.post(apiUrls.DO_FORBID_CREATE, payload)
    },
}
