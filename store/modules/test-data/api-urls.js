const BASE_URL = '/v1/data-test'

export default {
    DO_LIST: `${BASE_URL}/all`, //get
    DO_LIST_PAGING: `${BASE_URL}/page/{pageNum}`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_CREATE_COMMENT: `${BASE_URL}/comment/document-id/{id}`, //post
    DO_UPDATE: `${BASE_URL}/{id}`, //put
    DO_DELETE: `${BASE_URL}/{id}`, //del
    DO_CREATE: `${BASE_URL}/new`, //post
}

/* 기본 주소 설정, 바뀌는 주소 설정하기 */
