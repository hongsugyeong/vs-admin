export default {
    DO_LIST: 'test-data/doList',
    DO_LIST_PAGING: 'test-data/doListPaging',
    DO_DETAIL: 'test-data/doDetail',
    DO_CREATE_COMMENT: 'test-data/doCreateComment',
    DO_UPDATE: 'test-data/doUpdate',
    DO_DELETE: 'test-data/doDelete',
    DO_CREATE: 'test-data/doCreate'
}

/* 행위에 대한 이름 짓기, api-urls에서 행위를 가져옴 */
