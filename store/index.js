import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import member from './modules/member'
import topic from "./modules/topic"
import comments from "./modules/comments"
import forbid from "./modules/forbid"
import ask from "./modules/ask";

export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    member,
    topic,
    comments,
    forbid,
    ask,
}
