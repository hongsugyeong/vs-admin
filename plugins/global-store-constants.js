import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import memberConstants from '~/store/modules/member/constants'
Vue.prototype.$memberConstants = memberConstants

import topicConstants from '~/store/modules/topic/constants'
Vue.prototype.$topicConstants = topicConstants

import commentsConstants from '~/store/modules/comments/constants'
Vue.prototype.$commentsConstants = commentsConstants

import forbidConstants from '~/store/modules/forbid/constants'
Vue.prototype.$forbidConstants = forbidConstants

import askConstants from '~/store/modules/ask/constants'
Vue.prototype.$askConstants = askConstants
